Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: apycula
Upstream-Contact: Pepijn de Vos <pepijndevos@gmail.com>
Source: https://pypi.org/project/Apycula/
Files-Excluded:
 examples/deser/oser10-tec0117.cst
 examples/deser/oser10-tlvds-tec0117.cst
 examples/deser/oser4-tec0117.cst
 examples/deser/oser4-tlvds-tec0117.cst
 examples/deser/oser8-tec0117.cst
 examples/deser/oser8-tlvds-tec0117.cst
 examples/deser/ovideo-tec0117.cst
 examples/deser/ovideo-tlvds-tec0117.cst
 examples/longwires/tec0117.cst
 examples/tec0117.cst
 legacy/example/example.cst
 legacy/iob/iob.cst.mk
 legacy/lut4/lut4.cst.mk
 legacy/iob/iob.sdc
 legacy/lut4/lut4.sdc
 Apycula.egg-info/*

Files: *
Copyright: 2019 Pepijn de Vos
License: MIT

Files:
 examples/attosoc/attosoc.v
 examples/himbaechel/tangnano20k/attosoc.v
 examples/attosoc/picorv32.v
 legacy/generic/attosoc/attosoc.v
 legacy/generic/attosoc/picorv32.v
Copyright:
 2015-2017, Clifford Wolf <clifford@clifford.at>
 2018, David Shah <dave@ds0.me>
License: ISC

Files: debian/*
Copyright: 2023 Daniel Gröber <dxld@darkboxed.org>
License: MIT

License: MIT
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 . 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

License: ISC
 Permission to use, copy, modify, and/or distribute this software for any
 purpose with or without fee is hereby granted, provided that the above
 copyright notice and this permission notice appear in all copies.
 .
 THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
